<?php
use Phalcon\Crypt as Crypt;
class Tools
{
    public static function getDate(){
        
        return date("Y-m-d");
    }
    public static function getDateTime($timestamp = false){
        
        return ($timestamp)?date("Y-m-d H:i:s",$timestamp):date("Y-m-d H:i:s");
    }
    public static function getLetsPayDateTime(){
        
        return date("YmdHis");
    }
    public static  function getRequestHeaders() {
        $headers = array();
        foreach($_SERVER as $key => $value) {
            if (substr($key, 0, 5) <> 'HTTP_') {
                continue;
            }
            $header = str_replace(' ', '-', ucwords(str_replace('_', ' ', strtolower(substr($key, 5)))));
            $headers[$header] = $value;
        }
        return $headers;
    }
    public static function getYouTubeTitle($video_id){
        $html = 'https://www.googleapis.com/youtube/v3/videos?id=' . $video_id . '&key=AIzaSyDPWny4ri_7xMwmiluQPZr4C98kRn5n3uc&part=snippet';
        $response = Tools::httpGet($html);
        $decoded = json_decode($response, true);
        foreach ($decoded['items'] as $items) {
             $title = $items['snippet']['title'];
             return $title;
        }
    }
    public static function Pages($Pages,$entries, $Offset = 20){

        $Pages['PageIndex'] =(!empty($Pages['PageIndex']))?$Pages['PageIndex']:1;
        $Pages['entries'] = $entries;
        $Pages['Limit'] = ceil($Pages['entries']  / $Offset );

        $PageStart = ($Pages['PageIndex'] - 5 < 1)? 1 : ($Pages['PageIndex'] - 5);
        $PageEnd = ($PageStart + 10 < $Pages['Limit'])? ($PageStart + 10) :$Pages['Limit'];

        for($i = $PageStart ; $i < $PageEnd ;$i++) $Pages['List'][] = $i;


        if($Pages['PageIndex'] > $Pages['Limit']) $Pages['PageIndex'] = $Pages['Limit'];

        $Pages['Offset'] = (($Pages['PageIndex'] -1 < 1)?0:($Pages['PageIndex'] -1) * $Offset ) ;

        return $Pages;
    }
    public static function httpGet($url, $json = false)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        $response = curl_exec($ch);
        curl_close($ch);
        return ($json) ? json_decode($response, true) : $response;
    }

    public static function httpPost($url, $input = [], $json = false)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        // curl_setopt ( $ch, CURLOPT_SAFE_UPLOAD, false );
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($input));
        //SSL
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);

        $response = curl_exec($ch);
        return ($json) ? json_decode($response, true) : $response;
    }

    public static function fix_array_Key($data, $key = "id")
    {
        if (empty($data)) return array();
        $data = array_map('self::fix_element_Key', $data, array_fill(0, count($data), $key));
        return $data;
    }


    public static function fix_element_Key($data, $key = "id")
    {

        if (is_array($key)) {

            return (!empty($key)) ? array_intersect_key($data, array_flip($key)) : array();
        }

        return (isset($data[$key])) ? $data[$key] : false;
    }

    public static function filterArray($Items, $key, $arrayValue, $include = true)
    {

        if (!is_array($Items) || !is_array($arrayValue)) return [];

        return array_filter($Items, function ($item) use ($key, $arrayValue, $include) {

            if (!isset($item[$key])) return false;

            if ($include)
                return (in_array($item[$key], $arrayValue));
            else
                return (!in_array($item[$key], $arrayValue));
        });
    }

    public static function b64encode($string)
    {
        $data = base64_encode($string);
        $data = str_replace(array('+', '/', '='), array('-', '_', ''), $data);
        return $data;
    }
    public static function b64decode($string)
    {
        $data = str_replace(array(' ', '-', '_'), array('+','+', '/'), $string);
        $mod4 = strlen($data) % 4;
        if ($mod4) {
            $data .= substr('====', $mod4);
        }
        return base64_decode($data);
    }

    public static function Crypt(String $string ,$decode = false,String $key="adonisnowman"){
        $crypt = new Crypt();
		$crypt->setCipher('AES-256-CFB');
        if($decode == false) return self::b64encode($crypt->encrypt($string, $key));
        else return $crypt->decrypt(self::b64decode($string), $key);
    }

    public static function getFileList($dir = "", $reg = false)
    {
        if(!file_exists($dir)) return false;
        
        $Return = [];
        $dir = dir($dir);
        if($reg) $reg = "/(?P<reg>$reg)/";
        while (false !== ($entry = $dir->read())) {

            if ($entry == '.' || $entry == "..") continue;
            if($reg) {                
                preg_match($reg, $entry, $matchs);
                if(!empty($matchs['reg']))$Return[] = $matchs['reg'];
            }
            else $Return[] = $entry;
        }
        array_filter($Return);
            return $Return;
    }
}
